/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   number_of_number_in_str.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/16 19:51:10 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/04/16 19:58:31 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
**		This function count the number of numbers in a string but if the string
**		is false
**	(presence of a non numerique character or space between signe and nmber)
*/

int		number_of_number_in_str(char *tab)
{
	int		i;
	int		count;

	i = 0;
	count = 0;
	while (tab[i])
	{
		if (tab[i] != ' ')
		{
			if (tab[i] == '-' || tab[i] == '+')
				++i;
			if (tab[i] < '0' || tab[i] > '9')
				return (-1);
			while (tab[i] >= '0' && tab[i] <= '9')
				++i;
			++count;
		}
		else
			++i;
	}
	return (count);
}
