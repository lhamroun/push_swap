/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo_ps.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 20:51:16 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/06/06 19:37:17 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

char		*find_best_algo(t_pil s)
{
	char	*t;

	if (s.n < 4)
		t = algo_small_sort(&s);
	else if (s.n < 7)
		t = bad_quick_sort(s);
	else
		t = quick_sort(s, NULL);
	return (t);
}
